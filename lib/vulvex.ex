defmodule Vulvex do
  require HTTPoison
  require Poison

  def main(argv) do
    argv |> parse_args |> process
  end

  defp parse_args(args) do
    parse = OptionParser.parse_head(args, switches: [help: :boolean], aliases: [h: :help])
      case parse do
        {[], [], []}          -> :help
        {[ help: true], _, _} -> :help
        {[], [], [_]}  -> :negative
        {[], n, []} when n >= 1 -> n
    end
  end

  defp process(:help) do
    IO.puts """
    Fetches ASCII vulvas from the VulvAPI, located at http://vulvapi.asphodelium.eu
    usage: ./vulvex <integer>
    """
  end

  defp process(n) do
    HTTPoison.start
    url = "https://vulvapi.herokuapp.com/vulvas/#{n}"
    url |> HTTPoison.get! |> parse_vulvas
  end


  defp parse_vulvas(data) do
    vulvas = Poison.Parser.parse!(data.body)
    Map.values(vulvas)
      |> List.flatten
        |> Enum.map(fn(x) -> IO.puts(x) end)
  end
end
